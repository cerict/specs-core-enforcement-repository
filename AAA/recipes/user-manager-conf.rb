#
# Cookbook Name:: specs-enforcement-aaa
# Recipe:: default
#
# Copyright 2016
#
# All rights reserved - Do Not Redistribute
#
# authors: Pasquale De Rosa - Pierluca Nardone

include_recipe 'mysql::default'

#Create openLDAP folder
bash "create folder" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    mkdir openLDAP
    EOH
    not_if { ::File.exists?("/opt/openLDAP") }
end

#
# INSTALL Berkley DB (OpenLDAP dependency)
#
# Download dbInst
cookbook_file  "db-5.3.15.tar.gz" do
    path "/opt/openLDAP/db-5.3.15.tar.gz"
    action :create
end

# untar dbInst tar file
bash "untar_dbInst" do
    user "root"
    cwd  "/opt/openLDAP"
    code <<-EOH
    tar -zxvf /opt/openLDAP/db-5.3.15.tar.gz
    rm /opt/openLDAP/db-5.3.15.tar.gz
    EOH
    not_if { ::File.exists?("/opt/openLDAP/db-5.3.15") }
end

# install dbInst
bash "install_dbInst" do
    user "root"
    cwd  "/opt/openLDAP/db-5.3.15/build_unix"
    code <<-EOH
    zypper -n in gcc gcc-c++
    zypper -n in make
    
    ../dist/configure --prefix=/usr      \
    --enable-compat185 \
    --enable-dbm       \
    --disable-static   \
    --enable-cxx       &&
    make
    
    make docdir=/usr/share/doc/db-5.3.15 install &&
    chown -v -R root:root                        \
    /usr/bin/db_*                          \
    /usr/include/db{,_185,_cxx}.h          \
    /usr/lib/libdb*.{so,la}                \
    /usr/share/doc/db-5.3.15
    
    touch /opt/openLDAP/db-5.3.15/executed
    EOH
    not_if { ::File.exists?("/opt/openLDAP/db-5.3.15/executed") }
end

#
# INSTALL OPENLDAP
#
# Download openLdap
cookbook_file  "openldap-2.4.42.tgz" do
    path "/opt/openLDAP/openldap-2.4.42.tgz"
    action :create
end

# untar openLdap tar file
bash "untar_openLDAP" do
    user "root"
    cwd  "/opt/openLDAP"
    code <<-EOH
    tar -zxvf /opt/openLDAP/openldap-2.4.42.tgz
    rm /opt/openLDAP/openldap-2.4.42.tgz
    EOH
    not_if { ::File.exists?("/opt/openLDAP/openldap-2.4.42") }
end

# install openLdap
bash "install_openLDAP" do
    user "root"
    cwd  "/opt/openLDAP/openldap-2.4.42"
    code <<-EOH
    zypper -n in gcc gcc-c++
    zypper -n in make
    zypper -n in groff.x86_64
    
    ./configure --enable-slapd
    make depend
    make
    make install
    
    touch /opt/openLDAP/openldap-2.4.42/executed
    EOH
    not_if { ::File.exists?("/usr/local/lib/slapd") }
end

#
# CONFIGURE OPENLDAP
#
# Download openLdapConfig
cookbook_file  "openLDAPconfig.tar.gz" do
    path "/opt/openLDAP/openLDAPconfig.tar.gz"
    action :create
end

# untar openLdapConfig tar file
bash "untar_openLDAP_config" do
    user "root"
    cwd  "/opt/openLDAP"
    code <<-EOH
    tar -zxvf /opt/openLDAP/openLDAPconfig.tar.gz
    rm /opt/openLDAP/openLDAPconfig.tar.gz
    EOH
    not_if { ::File.exists?("/opt/openLDAP/openLDAPconfig") }
end

# set the configuration
bash "configure_openLDAP_config" do
    user "root"
    cwd  "/opt/openLDAP/openLDAPconfig"
    code <<-EOH
    cp /opt/openLDAP/openLDAPconfig/slapd.conf /usr/local/etc/openldap/
    cp -r dc\=specs\,dc\=eu /usr/local/var/openldap-data/
    touch /opt/openLDAP/openLDAPconfig/executed
    EOH
    not_if { ::File.exists?("/opt/openLDAP/openLDAPconfig/executed") }
end

#
# XACML CONFIGURATION
#
# Download XACMLpolicy
cookbook_file  "xacml_policy.tar.gz" do
    path "/opt/xacml_policy.tar.gz"
    action :create
end

# untar XACMLpolicy tar file
bash "untar_XACMLpolicy" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    tar -zxvf /opt/xacml_policy.tar.gz
    rm /opt/xacml_policy.tar.gz
    EOH
    not_if { ::File.exists?("/opt/xacml_policy") }
end

#
# MYSQL Configuration
#

# Create mySQLschema folder
bash "create_mySQLschema_folder" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    mkdir mySQLschema
    EOH
    not_if { ::File.exists?("/opt/mySQLschema") }
end

# Import app_db SQL schema
#

cookbook_file  "app_db_schema.tar.gz" do
    path "/opt/mySQLschema/app_db_schema.tar.gz"
    action :create
end

bash "untar_app_db_schema" do
    user "root"
    cwd  "/opt/mySQLschema"
    code <<-EOH
    tar -zxvf /opt/mySQLschema/app_db_schema.tar.gz
    rm /opt/mySQLschema/app_db_schema.tar.gz
    EOH
    not_if { ::File.exists?("/opt/mySQLschema/app_db_schema") }
end

execute 'import_app_db_schema' do
    user "root"
    cwd "/opt/mySQLschema/app_db_schema"
    command <<-EOF
    echo "create database app_db" | mysql -u root -pprova
    mysql -u root -pprova app_db < schema.sql
    touch /opt/mySQLschema/app_db_schema/executed
    EOF
    not_if { ::File.exists?("/opt/mySQLschema/app_db_schema/executed") }
end

# retry install openLdap
bash "retry_install_openLDAP" do
    user "root"
    cwd  "/opt/openLDAP/openldap-2.4.42"
    code <<-EOH
    
    ./configure --enable-slapd
    make depend
    make
    make install
    
    touch /opt/openLDAP/openldap-2.4.42/executed
    EOH
    not_if { ::File.exists?("/usr/local/lib/slapd") }
end


#START slapd (OpenLDAP Server)
bash "execute_openLDAP" do
    user "root"
    cwd  "/usr/local/lib"
    code <<-EOH
    nohup ./slapd -d -1 -f /usr/local/etc/openldap/slapd.conf -h ldap://localhost:389 &>/dev/null &
    touch /opt/openLDAP/openldap-2.4.42/execution
    EOH
    only_if { ::File.exists?("/usr/local/lib/slapd") }
end