default['postgresql']['config']['listen_addresses'] = '*'
default['postgresql']['config']['wal_level'] = 'hot_standby'
default['postgresql']['config']['archive_mode'] = 'on'
default['postgresql']['config']['archive_command'] = 'cd .'
default['postgresql']['config']['max_wal_senders'] = '1'
default['postgresql']['config']['hot_standby'] = 'on'

default['replication']['user'] = 'rep'
default['replication']['password'] = 'useagudpasswd'
default['postgresql']['password']['postgres'] = 'postgres'

default['event_hub_url'] = 'http://172.16.118.86:8081/events/specs'
default['postgresql']['port'] = '5432'
default['postgresql_backup']['port'] = '5432'
default['rate'] = 1