include_recipe "DBB::install_postgresql"

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first
if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end
vms = plan['pools'].first['vms']
master_ip = 'localhost'
slave_ip = 'localhost'
e2ee_ip = 'localhost'
e2ee_backup_ip = 'localhost'

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_main_db'
			master_ip = ip
		end
		if component['component_id'] == 'DBB_backup_db'
			slave_ip = ip
		end
		if component['component_id'] == 'DBB_main_server'
			e2ee_ip = ip
		end
		if component['component_id'] == 'DBB_backup_server'
			e2ee_backup_ip = ip
		end
	end
end

node.default['postgresql']['pg_hba'] = [
	{:comment => '# IPv4 configuration', :type => 'host', :db => 'all', :user => 'all', :addr => '127.0.0.1/32', :method => 'md5'},
	{:comment => '# IPv6 configuration', :type => 'host', :db => 'all', :user => 'all', :addr => '::1/128', :method => 'md5'},
	{:comment => '# E2EE configuration', :type => 'host', :db => 'all', :user => 'postgres', :addr => "#{e2ee_ip}/32", :method => 'md5'},
	{:comment => '# E2EE backup configuration', :type => 'host', :db => 'all', :user => 'postgres', :addr => "#{e2ee_backup_ip}/32", :method => 'md5'},
	{:comment => '# Replication configuration master', :type => 'host', :db => 'replication', :user => node['replication']['user'], :addr => "#{master_ip}/32", :method => 'md5'},
	{:comment => '# Replication configuration slave', :type => 'host', :db => 'replication', :user => node['replication']['user'], :addr => "#{slave_ip}/32", :method => 'md5'}
]

service 'postgresql' do
	action :restart
end

execute 'Create replication user' do
	role_exists = %(psql -c "SELECT rolname FROM pg_roles WHERE rolname='#{node['replication']['user']}'" | grep #{node['replication']['user']})
	command %Q(psql -c "CREATE USER #{node['replication']['user']} REPLICATION LOGIN CONNECTION LIMIT 1 ENCRYPTED PASSWORD '#{node['replication']['password']}';")
	not_if role_exists, user: 'postgres'
	user 'postgres'
	action :run
end