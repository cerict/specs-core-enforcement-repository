package 'git'
package 'python-pip'

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first
if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end
vms = plan['pools'].first['vms']
master_ip = ''
slave_ip = ''

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['recipe'] == 'postgresql_master'
			master_ip = ip
		end
		if component['recipe'] == 'postgresql_slave'
			slave_ip = ip
		end
	end
end
node.default['postgresql']['ip'] = master_ip
node.default['postgresql_backup']['ip'] = slave_ip

bash 'install_requests' do
	code <<-EOH
		python -m pip install requests
	EOH
end

bash 'clone_monitoring_repository' do
    cwd '/opt'
    code <<-EOH
        git clone https://bitbucket.org/specs-team/specs-mechanism-monitoring-e2ee-adapter
    EOH
end