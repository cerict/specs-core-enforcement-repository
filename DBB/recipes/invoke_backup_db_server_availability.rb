include_recipe "DBB::clone_monitoring_component"

bash 'check_if_postgresql_backup_is_running' do
	code <<-EOH
		python /opt/specs-mechanism-monitoring-e2ee-adapter/monitor.py -i database_backup #{node['postgresql_backup']['ip']} #{node['postgresql_backup']['port']} #{node['event_hub_url']}
	EOH
end