include_recipe "DBB::clone_monitoring_component"

bash 'check_if_postgresql_is_running' do
	code <<-EOH
		python /opt/specs-mechanism-monitoring-e2ee-adapter/monitor.py -i database #{node['postgresql']['ip']} #{node['postgresql']['port']} #{node['event_hub_url']}
	EOH
end