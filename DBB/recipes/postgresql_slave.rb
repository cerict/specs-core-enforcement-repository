if File.exist?('/tmp/postgresql_slave') then
	puts "Recipe already ran"
	return
end

if !File.exist?('/tmp/install_postgresql') then
	include_recipe "DBB::install_postgresql"
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first
if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end
vms = plan['pools'].first['vms']
master_ip = ''
slave_ip = ''
e2ee_backup_ip = ''

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_main_db'
			master_ip = ip
		end
		if component['component_id'] == 'DBB_backup_db'
			slave_ip = ip
		end
		if component['component_id'] == 'DBB_backup_server'
			e2ee_backup_ip = ip
		end
	end
end

service 'postgresql' do
	action :stop
end

execute 'Clear directory' do
	cwd '/var/lib/pgsql/data'
	user 'postgres'
	command 'rm -fr *'
end

file '/var/lib/pgsql/.pgpass' do
	user 'postgres'
	group 'postgres'
	mode '0600'
	content "#{master_ip}:5432:*:#{node['replication']['user']}:#{node['replication']['password']}"
end

execute 'Initial backup' do
	cwd '/var/lib/pgsql/data'
	user 'postgres'
	command "pg_basebackup -h #{master_ip} -p 5432 -D . --username=#{node['replication']['user']} --no-password"
end

file '/var/lib/pgsql/data/recovery.conf' do
	user 'postgres'
	group 'postgres'
	content "standby_mode = 'on'
			primary_conninfo = 'host=#{master_ip} port=5432 user=#{node['replication']['user']} password=#{node['replication']['password']}'
			trigger_file = '/tmp/postgresql.trigger.5432'
			"
end

ruby_block 'Modify pg_hba' do
 	block do
 		file = Chef::Util::FileEdit.new("/var/lib/pgsql/data/pg_hba.conf")
		file.search_file_replace_line("host\s*all\s*postgres.*\/32\s*md5", "host all postgres #{e2ee_backup_ip}/32 md5")
 		file.write_file
 	end
 end

service 'postgresql' do
	action :restart
end

file '/tmp/postgresql_slave'