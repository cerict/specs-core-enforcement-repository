if File.exist?('/tmp/turn_to_master') then
	puts "Recipe already ran"
	return
end

file '/tmp/postgresql.trigger.5432'


plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first
if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end
vms = plan['pools'].first['vms']
slave_ip = 'localhost'
e2ee_backup_ip = 'localhost'

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_backup_db'
			slave_ip = ip
		end
		if component['component_id'] == 'DBB_backup_server'
			e2ee_backup_ip = ip
		end
	end
end


ruby_block 'Modify pg_hba' do
 	block do
 		file = Chef::Util::FileEdit.new("/var/lib/pgsql/data/pg_hba.conf")
		file.insert_line_if_no_match("host\s*replication\s*rep\s*#{slave_ip}\/32\s*md5", "host replication rep #{slave_ip}/32 md5")
		file.insert_line_if_no_match("host\s*all\s*postgres\s*#{e2ee_backup_ip}\/32\s*md5", "host all postgres #{e2ee_backup_ip}/32 md5")
 		file.write_file
 	end
 end
 
service 'postgresql' do
	action :restart
end


file '/tmp/turn_to_master'
	