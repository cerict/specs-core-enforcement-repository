specs-monitoring-ossec Cookbook
====================
This cookbook contains two recipes: the agent one allows to install the 
Ossec-agent, while the server one allows to install the ossec server and 
registers the agents that have been created, on the Ossec-Server.    

Requirements
------------
1) The agent recipe has to be executed on all the agent nodes before the 
execution of the server recipe.

2) OSSEC-SERVER: the target machine needs the udp port 1514 to be 
opened. On Amazon AMI: you need to:
- access to amazon console;
- add an inbound rule into the security group [Custom UDP rule].   

3) Ossec analyze the apache2, nginx and system syslog.
   The apache2 access log and error log have to located into the following paths: /opt/apache2/logs/access_log , /opt/apache2/logs/error_log  
   The nginx access log and error log have to located into following paths :       /opt/nginx/logs/access.log   , /opt/nginx/logs/error.logs
   The system syslog have to located into following paths : /var/log/messages. NB if syslog is not running is started.

4) The server node have to have the some ssh private key of the agent nodes

5) The target machine need the OpenSuse OS 64 bit.

Attributes
----------
For each recipe,The attributes have to be passed a implementation plan id : 

{"implementation_plan_id":"ID_VALUE"}

Usage                              
-----------

#### create implementation plans data bag
if not exist, create a data bag "implementation_plans"  with follow command: knife data bag create data  

#### add implementation plan
add a item: knife data bag from file implementation_plan.json 

  example of  implementation_plan with one ossec-server and two ossec-agent:

``` 
   {
  "id":"1154982",
  "context": {
    "slaID": "$SLA-ID",
    "planID": "$PLAN-ID",
    "annotations": [],
    "monitoring-core-ip": "192.168.1.150",
    "monitoring-core-port": "5000"
  },
  "IaaS": {
    "VMs": {
      "nodes-access-credentials-reference": "$$ filled by broker or $by plan",
      "nodes": [
        {
          "node-id": "server-node",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": true,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "0.0.0.0/0",
                "source-nodes": [],
                "interface": "public",
                "proto": "TCP",
                "port-list": "22,1514"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "0.0.0.0/0",
                "destination-nodes": [],
                "interface": "public,private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-1",
              "cookbook": "specs-monitoring-ossec",
              "name": "server",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.58.95"
          ],
          "public-ip": "$$ filled by broker"
        },
        {
          "node-id": "agent-node1",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-3",
              "cookbook": "specs-monitoring-ossec",
              "name": "agent",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.60.225"
          ],
          "public-ip": "$$ filled by broker"
        },
        {
          "node-id": "agent-node2",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-5",
              "cookbook": "specs-monitoring-ossec",
              "name": "agent",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.51.223"
          ],
          "public-ip": "$$ filled by broker"
        },
      ]
    },
    "STORAGEs": []
  },
  "providers": [
    {
      "id": "aws-east",
      "name": "aws-ec2",
      "zone": "us-east-1"
    }
  ],
}

```

#### specs-monitoring-ossec::agent

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['specs-monitoring-ossec::agent']' -j '{ "implementation_plan_id":"<id_value>"}' 

example : knife bootstrap 192.168.1.101 -x root -P specs --node-name agent-node1 --run-list 'recipe['specs-monitoring-ossec::agent']' -j '{ "implementation_plan_id":"1154982"}'
          knife bootstrap 192.168.1.103 -x root -P specs --node-name agent-node2 --run-list 'recipe['specs-monitoring-ossec::agent']' -j '{ "implementation_plan_id":"1154982"}'

#### specs-monitoring-ossec::server

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['specs-monitoring-ossec::server']' -j '{ '{ 
"implementation_plan_id":"<id_value>"}'}'

example : knife bootstrap 192.168.1.103 -x root -P specs --node-name server-node --run-list 'recipe['specs-monitoring-ossec::server']' -j '{ "implementation_plan_id":"1154982"}'


Contributors
------------------
Vincenzo Cinque & Giancarlo Capone


License and Authors
-------------------
Authors: Cinque Vincenzo & Capone Giancarlo.
License :
//# Copyright (C) 2014 - 2016 CeRICT
//# Developed for the SPECS Project FP7 - GA 610795 - 
//# Authors: 
//# %%
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#      http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
