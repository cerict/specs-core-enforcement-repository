// Procedura per prendere l'indirizzo ip da argomento. Delle volte mi ha bloccato la porta 4880 e non funzionava più il modulo. Nel caso, decommentare tutta la procedura per prendere l'argomento, ed inserire l'indirizzo di HEKA qui in HOSTFWD a mano. Oppure togliere solo process.kill()
var myArgs = process.argv.slice(2);

if(myArgs[0]===undefined){

        console.log("passa l'indirizzo ip di HEKA come argomento\n"+
                'es. "node ossec-adapter.js 54.93.30.151 5000 123456789"\n');

	process.kill();
}
if(myArgs[1]===undefined){

        console.log("passa port  di HEKA come argomento\n"+
                'es. "node ossec-adapter.js 54.93.30.151 5000 123456789"\n');

        process.kill();
}
if(myArgs[2]==undefined){

	console.log("passa identificativo dello SLA\n"+
		'es. "node ossec-adapter.js 54.93.30.151 5000 123456789"\n');
	process.kill();
}

/////////////////////////////////////////////////////////////////////
var PORT = 4880;
var PORTFWD = myArgs[1];
var HOST = '127.0.0.1';
var HOSTFWD = myArgs[0];
var SLAID = myArgs[2];
var olddate = null;
var report = null;
var fupdate = null;

var dgram = require('dgram');
var fs = require('fs');
var server = dgram.createSocket('udp4');
var http = require('http');

if (myArgs[3]===undefined){
      fupdate = 10000;
}
else fupdate = myArgs[3];

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
    console.log('SLAID: '+SLAID);
    console.log('aggiornerò con frequenza '+fupdate+' ms.');
    // prendo la data attuale e la trasformo in secondi
    var date = new Date();
    olddate = date;
    report = 'specs_report_'+date;
    fs.writeFile('/var/log/'+report,'REPORT OSSEC \n');
    var seconds = (new Date().getTime())/1000;
    console.log(seconds);
    // preparo il json da inviare
    var jsonfy = JSON.stringify(
			{       Object:"",
				Timestamp: seconds, 
				Labels:["sla_id_"+SLAID,"security_metric_dDoS_scan_frequency_m16"],
				Component:"ossec_server",
				Token:null,
				Type:"integer", 
				Data:fupdate.toString()
			});
    console.log(jsonfy);
	
    var options = {
    		host: HOSTFWD,
    		port: PORTFWD,
    		path: '/events/event',
    		method: 'POST',
    };
	
    var req = http.request(options, function(response) {
	console.log('esito positivo');
	if(response.statusCode!==200) console.log('codice ritorno diverso da 200');
    });
  
    req.on('error', function(err) {console.log('request error');});

    req.write(jsonfy);
    req.end();
  });


server.on('message', function (message, remote) {
    console.log(remote.address + ':' + remote.port +' - ' + message);
   
	var client = dgram.createSocket('udp4');
	
	// regex per estrarre dal json inviato da ossec l'alert
	var regexjson = /{.*}/g;
	var regexinfo = /<.*(?={)/g;
	// estraggo informazioni utili dal json inviato da ossec
	var seljson = regexjson.exec(message)[0];
        var json = JSON.parse(seljson);
	console.log(seljson);
	// aggiungo il json relativo all'alert arrivato nel report
	fs.appendFile('/var/log/'+report,seljson+'\n');
	
});

setInterval(function() {
  console.log('Sono passati '+fupdate+' ms ed invio');
  var seconds = (new Date().getTime())/1000;
  console.log(seconds);
  // preparo il json da inviare
  var jsonfy = JSON.stringify(
			{       Object:"",
				Timestamp: seconds, 
				Labels:["sla_id_"+SLAID,"security_metric_dDoS_scan_frequency_m16"],
				Component:"ossec_server",
				Token:null,
				Type:"integer", 
				Data:fupdate.toString()
			});
  console.log(jsonfy);
	
  var options = {
    		host: HOSTFWD,
    		port: PORTFWD,
    		path: '/events/event',
    		method: 'POST',
  };
	
  var req = http.request(options, function(response) {
	console.log('esito positivo');
	if(response.statusCode!==200) console.log('codice ritorno diverso da 200');
  });
  
  req.on('error', function(err) {console.log('request error');});

  req.write(jsonfy);
  req.end();
  var date = new Date();
  olddate = date;
  report = 'specs_report_'+date;
  fs.writeFile('/var/log/'+report,'REPORT OSSEC \n');
}, fupdate) ;

server.bind(PORT, HOST);