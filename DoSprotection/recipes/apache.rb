#
# Cookbook Name:: DoSprotection
# Recipe:: apache
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# install apache2

package 'apache2' do
action :install
end

# install php5

package 'apache2-mod_php5' do
action :install
end

# Edit apache2 configuration file

cookbook_file  "apache2" do
   path "/etc/sysconfig/apache2"
   action :create
end

cookbook_file  "httpd.conf" do
   path "/etc/apache2/httpd.conf"
   action :create
end

cookbook_file  "default-server.conf" do
   path "/etc/apache2/default-server.conf"
   action :create
end

service 'apache2.service' do
action [:enable,:start]
end



 





