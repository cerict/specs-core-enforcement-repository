#
# Cookbook Name:: specs-monitoring-ossec
# Recipe:: server
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe"DoSprotection::apache"

# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
#plan = search("SPECS","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then
  puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
  exit
end

# get EventHub information
# old plan
#EventHubIP=plan['context']['monitoring-core-ip']
#EventHubPort=plan['context']['monitoring-core-port']
#new plan
EventHubIP=plan['monitoring_core_ip']
EventHubPort=plan['monitoring_core_port']

if EventHubIP == nil or EventHubPort == nil
  puts "Insert monitoring-core-ip and monitoring-core-port in context tag of implementation plan"
  exit
end 

# Download package ossec server
cookbook_file  "Server.tar.gz" do
   path "/tmp/ossec-server.tar.gz"
   action :create
end
# Download installer script
cookbook_file "scripts.tar.gz" do
   path "/tmp/ossec-scripts.tar.gz"
   action :create
end
# Download ossec adapter script
cookbook_file "ossec-adapter.js" do  
   path "/opt/ossec-adapter.js"
   action :create
end

SlaID=plan['sla_id']
puts "Lo SLA ID è: #{SlaID}"

plan['slos'].each do | slo |
	if slo['metric_id']=="dDoS_scan_frequency_m16"
		Frequency=slo['value']
		puts "frequency:"
		puts "#{Frequency}"
	end
end

# unzip tar file and execute installer script
bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH 
          tar -zxvf /tmp/ossec-server.tar.gz
          rm /tmp/ossec-server.tar.gz
          tar -xvf /tmp/ossec-scripts.tar.gz
          rm /tmp/ossec-scripts.tar.gz
          chmod +x ossec-scripts/Installsyslog.sh
          #if syslog is not installed or not running then it is installed and started
          ossec-scripts/Installsyslog.sh
          chmod +x ossec-scripts/InstallServer.sh 
          ossec-scripts/InstallServer.sh
          ossec/bin/ossec-control start          
          chmod +x ossec-scripts/InstallNodeJs.sh
          ossec-scripts/InstallNodeJs.sh 
		  node ossec-adapter.js  #{EventHubIP} #{EventHubPort} #{SlaID} #{Frequency} > /dev/null &
          chmod +x ossec-scripts/ossec-batch-manager.pl          
          chmod +x ossec-scripts/AddAgent.sh
          rm -r ossec-server
        EOH
        not_if { ::File.exists?("/opt/ossec") }
end

bash "get_ssh_credentials" do
  user "root"
  cwd "/root"
  code <<-EOH
    if [ -f /mos/etc/mos/environment.sh ];then
      zypper -n install wget 
      mkdir -p /root/.ssh
      source /mos/etc/mos/environment.sh
      server=$(cat /etc/chef/client.rb | grep chef_server_url)
      IFS=// read -a myarray <<< "$server"
      server_name=$(echo ${myarray[2]})
      complete_name=$(echo http://$server_name:81/mos/var/mos-chef-server-core/ssh-key -O /root/.ssh/id_rsa)
      wget $complete_name
      complete_name2=$(echo http://$server_name:81/mos/var/mos-chef-server-core/ssh-key.pub -O /root/.ssh/id_rsa.pub)
      wget $complete_name2
      chmod 600 /root/.ssh/id_rsa
      chmod 644 /root/.ssh/id_rsa.pub
      cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
      chmod 600 /root/.ssh/authorized_keys
    fi
  EOH
  not_if { ::File.exists?("/root/.ssh/id_rsa") }
  returns [0, 1]
end

# get ossec agents ip address
# old plan
#plan['IaaS']['VMs']['nodes'].each do | node |
#  node['recipes'].each do | recipe |
#   if (recipe['cookbook']=="specs-monitoring-ossec" and recipe['name']=="agent")
#	name=node['node-id']
#        IP_Address=node['private-ips'].first
#	# Add Agent
#	bash "add_agent" do
#        user "root"
#        cwd  "/opt"
#        code <<-EOH
#        ossec-scripts/AddAgent.sh #{name} #{IP_Address}
#        ossec/bin/ossec-control restart
#        EOH
#        end
#   end
#  end
#end

Ip=nil

plan['pools'].each do | pool |
  pool['vms'].each do | vm |
    vm['components'].each do | component |
	      if (component['cookbook']=="WebPool" and component['recipe']=="haproxy")
		Ip = component['private_ips'].first
	      end
	end
   end
end


# new plan
plan['pools'].each do | pool |
  pool['vms'].each do | vm |
    vm['components'].each do | component |
	      if (component['cookbook']=="DoSprotection" and component['recipe']=="agent")
		    IP_Address = component['private_ips'].first
			if(Ip==IP_Address)
				Id="001"
		    else
				App=IP_Address.split(".")
                Id=App[3]
		    end
			puts "sto per aggiungere un agent con ID :#{Id} ed IP :#{IP_Address}" 
		    # Add Agent
		    bash 'add_agent' do
		          user "root"
		          cwd  "/opt"
		          code <<-EOH
		            ossec-scripts/AddAgent.sh #{IP_Address} #{Id} #{IP_Address}
		          EOH
		          not_if "grep -q #{IP_Address} /opt/ossec/etc/clients.key"
		    end
	       end
    end
  end
end

bash 'restart_server' do
	user "root"
	cwd  "/opt"
	code <<-EOH
		sleep 10
		ossec/bin/ossec-control restart
	EOH
end

# Download package ossec-wui
cookbook_file  "ossec-wui-0.8.tar.gz" do
   path "/tmp/ossec-wui-0.8.tar.gz"
   action :create
end

# install ossec-wui
bash "install_ossec_wui" do
  user "root"
  code <<-EOH
    tar -zxvf /tmp/ossec-wui-0.8.tar.gz
    rm /tmp/ossec-wui-0.8.tar.gz
    mv ossec-wui-0.8 /srv/www/htdocs/ossec-wui
    chmod +x /srv/www/htdocs/ossec-wui/setup.sh
    /srv/www/htdocs/ossec-wui/setup.sh
    echo ossec:x:1000:wwwrun >> /etc/group
    chmod 770 /opt/ossec/tmp/
    chgrp nobody /opt/ossec/tmp/
    systemctl restart apache2.service
  EOH
  not_if { ::File.exists?("/srv/www/htdocs/ossec-wui/ossec_conf.php") }
end


