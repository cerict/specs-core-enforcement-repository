name             'E2EE'
maintainer       'XLAB'
maintainer_email 'aljaz.kosir@xlab.si'
license          'All rights reserved'
description      'Installs/Configures E2EE'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'postgresql'
depends 'golang'
depends 'database'
depends 'build-essential'
depends 'DBB'