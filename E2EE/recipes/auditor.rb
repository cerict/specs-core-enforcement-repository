package 'python-pip'
package 'screen'
package 'mercurial' do
	action :remove
end

if File.exist?('/tmp/auditor.pid') then
	puts "Auditor already running"
	exit
end

bash 'install_flask' do
	code <<-EOH
		python -m pip install flask
	EOH
end

bash 'Add repository for mercurial' do
	code <<-EOH
		zypper ar -f http://download.opensuse.org/repositories/devel:/tools:/scm/openSUSE_13.1/ devel-tools
	EOH
	ignore_failure true
end

package 'mercurial' do
    version "3.7.3"
end

directory "/opt/specs-mechanism-monitoring-e2ee-auditor" do
	recursive true
	action :delete
end

bash 'clone-mercurial-repository' do
	cwd '/opt'
	code <<-EOH
		hg clone https://bitbucket.org/specs-team/specs-mechanism-monitoring-e2ee-auditor
	EOH
	ignore_failure true
end

bash 'run_auditor' do
	cwd '/opt'
	code <<-EOH
		screen -m -S "Auditor" -d python specs-mechanism-monitoring-e2ee-auditor/auditor/auditorapi.py
		echo $(screen -ls | awk '/\.Auditor\t/ {print strtonum($1)}') > /tmp/auditor.pid
	EOH
end