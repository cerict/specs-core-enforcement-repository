package 'git'
package 'python-pip'

bash 'install_requests' do
	code <<-EOH
		python -m pip install requests
	EOH
end

bash 'clone_monitoring_repository' do
    cwd '/opt'
    code <<-EOH
        git clone https://bitbucket.org/specs-team/specs-mechanism-monitoring-e2ee-adapter
    EOH
end