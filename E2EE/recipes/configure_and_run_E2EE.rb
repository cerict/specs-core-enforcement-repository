#
# Cookbook Name:: e2ee
# Recipe:: configure_and_run_E2EE
# 	This recipe is a wrapper for setting up postgres backend for E2EE server
#
# Copyright 2016, XLAB
#
# All rights reserved - Do Not Redistribute
#

if File.exist?('/tmp/configure_and_run_E2EE') then
	puts "Recipe already ran"
	return
end

gopath = node['go']['gopath']
e2ee_path = "#{gopath}/src/github.com/xlab-si/e2ee-server"

# Run E2EE Server in background, redirect ouptut to file 
execute "e2ee-server" do
	command "#{node['go']['gobin']}/e2ee-server > #{node['e2ee']['log_file']} 2>&1 &"
	environment 'GOPATH' => gopath
end

file '/tmp/configure_and_run_E2EE'