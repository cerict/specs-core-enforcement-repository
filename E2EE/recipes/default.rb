#
# Cookbook Name:: e2ee
# Recipe:: default
#	Installs, tests and runs E2EE server
#
# Copyright 2016, XLAB
#
# All rights reserved - Do Not Redistribute
#

gopath = node['go']['gopath']
e2ee_path = "#{gopath}/src/github.com/xlab-si/e2ee-server"

# Install E2EE server
execute "Installing E2EE Server" do
	cwd "#{e2ee_path}"
	command "go install"
	environment ({
    'PATH' => "/usr/local/go/bin:#{ENV['PATH']}",
    'GOPATH' => gopath
	}) 
end


# Run E2EE Server in background, redirect ouptut to file 
execute "e2ee-server" do
	command "#{node['go']['gobin']}/e2ee-server > #{node['e2ee']['log_file']} 2>&1 &"
	environment 'GOPATH' => gopath
end