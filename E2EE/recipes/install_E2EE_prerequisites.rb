#
# Cookbook Name:: e2ee
# Recipe:: install_E2EE_prerequisites
# 	This recipe is a wrapper for setting up postgres backend for E2EE server
#
# Copyright 2016, XLAB
#
# All rights reserved - Do Not Redistribute
#

include_recipe "database::postgresql"

gopath = node['go']['gopath']
e2ee_path = "#{gopath}/src/github.com/xlab-si/e2ee-server"

# Install E2EE server
execute "Installing E2EE Server" do
	cwd "#{e2ee_path}"
	command "go install"
	environment ({
    'PATH' => "/usr/local/go/bin:#{ENV['PATH']}",
    'GOPATH' => gopath
	}) 
end