bash 'restart_crypton_server' do
	cwd '/opt'
	code <<-EOH
		kill $(cat /tmp/crypton.pid)
		screen -wipe Crypton
		cd specs-mechanism-enforcement-e2ee-server
		make reset
		service postgresql restart
		screen -m -d -S "Crypton" ./server/bin/cli.js run
		echo $(screen -ls | awk '/\.Crypton\t/ {print strtonum($1)}') > /tmp/crypton.pid
	EOH
end