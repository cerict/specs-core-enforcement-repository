bash 'restart_auditor' do
	cwd '/opt'
	code <<-EOH
		kill $(cat /tmp/auditor.pid)
		screen -wipe Auditor
		screen -m -S "Auditor" -d python specs-mechanism-monitoring-e2ee/auditor/auditorapi.py
		echo $(screen -ls | awk '/\.Auditor\t/ {print strtonum($1)}') > /tmp/auditor.pid
	EOH
end