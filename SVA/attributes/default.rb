default['postgresql']['pg_hba'] = [
	{:comment => '# IPv4 configuration', :type => 'host', :db => 'all', :user => 'all', :addr => '127.0.0.1/32', :method => 'md5'},
	{:comment => '# IPv6 configuration', :type => 'host', :db => 'all', :user => 'all', :addr => '::1/128', :method => 'md5'}
]
default['django']['port'] = '8000'
default['project_path'] = '/opt/SVA'
default['python_path'] = '/usr/bin/python'
default['build-essential']['compile_time'] = true
