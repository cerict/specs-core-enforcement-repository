#!/bin/bash

echo "Starting script pacchetto libraries-scanner-manager..."


PATH_FILE_BASH=/root/.bashrc
PATH_FILE_BASH_LD_LIBRARY=/etc/bash.bashrc

echo "1 - Set PKG_CONFIG_PATH variable"
echo export "PKG_CONFIG_PATH=\$PKG_CONFIG_PATH\":/opt/openvas-libs/sqlite/lib64/pkgconfig/\"" >> $PATH_FILE_BASH

echo "2 - Set PATH variable"
echo export "PATH=\$PATH\":/opt/openvas-libraries-scanner-manager/sbin/:/opt/openvas-libs/nmap/bin/\"" >> $PATH_FILE_BASH

echo "3 - Set LD_LIBRARY_PATH variable"
echo export "LD_LIBRARY_PATH=\$LD_LIBRARY_PATH\":/opt/openvas-libraries-scanner-manager/lib/:/opt/openvas-libs/libgcrypt/lib64/:/opt/openvas-libs/libgpg-error/lib64/:/opt/openvas-libs/gpgme/lib64/:/opt/openvas-libs/gnutls/lib64/:/opt/openvas-libs/libpcap/lib64/\"" >> $PATH_FILE_BASH_LD_LIBRARY

echo "4 - Set CFLAGS variable"
echo export "CFLAGS=\$CFLAGS\" -I/opt/openvas-libs/sqlite/include\"" >> $PATH_FILE_BASH

echo "5 - Set LDFLAGS variable"
echo export "LDFLAGS=\$LDFLAGS\" -L/opt/openvas-libs/sqlite/lib64/\"" >> $PATH_FILE_BASH

echo "6 - Set all variables for this bash"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/opt/openvas-libs/sqlite/lib64/pkgconfig/"
export PATH="$PATH:/opt/openvas-libraries-scanner-manager/sbin/:/opt/openvas-libs/nmap/bin/"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/openvas-libraries-scanner-manager/lib/:/opt/openvas-libs/libgcrypt/lib64/:/opt/openvas-libs/libgpg-error/lib64/:/opt/openvas-libs/gpgme/lib64/:/opt/openvas-libs/gnutls/lib64/:/opt/openvas-libs/libpcap/lib64/"
export CFLAGS="$CFLAGS -I/opt/openvas-libs/sqlite/include/"
export LDFLAGS="$LDFLAGS -L/opt/openvas-libs/sqlite/lib64/"

echo "7 - Create openvas scanner and manager certificates"
openvas-mkcert -n -i
openvas-mkcert-client -n -i

echo "8 - Synchronize NVTs"
openvas-nvt-sync

echo "9 - Starting openvas scanner"
openvassd

  OPENVASSD_HOST=`netstat -A inet -ntlp 2> /dev/null | grep openvassd | awk -F\  '{print $4}' | awk -F: '{print $1}'`
  OPENVASSD_PORT=`netstat -A inet -ntlp 2> /dev/null | grep openvassd | awk -F\  '{print $4}' | awk -F: '{print $2}'`
  
  case "$OPENVASSD_HOST" in
    "0.0.0.0") echo "OK: OpenVAS Scanner is running and listening on all interfaces." ;;
    "127.0.0.1") echo "OK: OpenVAS Scanner is running and listening only on the local interface." ;;
    "") OPENVASSD_PROC=`ps -Af | grep "openvassd: waiting for incoming connections" | grep -v grep | wc -l`
        if [ $OPENVASSD_PROC -eq 0 ]
        then
          echo "ERROR: OpenVAS Scanner is NOT running!" ;
          echo "FIX: Start OpenVAS Scanner (openvassd)." ;
          OPENVASSD_PORT=-1 ;
        else
          echo "WARNING: OpenVAS Scanner seems to be run by another user!" ;
          echo "FIX: If intended this is OK (e.g. as root). But we can not determine the port." ;
          echo "FIX: You might face subsequent problems if not intended." ;
          OPENVASSD_PORT=1 ;
        fi
        ;;
  esac
  case $OPENVASSD_PORT in
    -1) ;;
    9391) echo "OK: OpenVAS Scanner is listening on port 9391, which is the default port." ;;
    *) echo "WARNING: OpenVAS Scanner is listening on port $OPENVASSD_PORT, which is NOT the default port!"
       echo "SUGGEST: Ensure OpenVAS Scanner is listening on port 9391." ;;
  esac
  
echo "10 - Rebuild openvas manager database"
openvasmd --rebuild

echo "11 - Create a user for openvas manager"
openvasmd --create-user $1
openvasmd --new-password $2 --user=$1

echo "12 - Starting openvas manager"
openvasmd 

  OPENVASMD_HOST=`netstat -A inet -ntlp 2> /dev/null | grep openvasmd | awk -F\  '{print $4}' | awk -F: '{print $1}'`
  OPENVASMD_PORT=`netstat -A inet -ntlp 2> /dev/null | grep openvasmd | awk -F\  '{print $4}' | awk -F: '{print $2}'`
  
  case "$OPENVASMD_HOST" in
    "0.0.0.0") echo "OK: OpenVAS Manager is running and listening on all interfaces." ;;
    "127.0.0.1") echo "WARNING: OpenVAS Manager is running and listening only on the local interface."
                 echo "This means that you will not be able to access the OpenVAS Manager from the"
                 echo "outside using GSD or OpenVAS CLI."
                 echo "SUGGEST: Ensure that OpenVAS Manager listens on all interfaces unless you want"
                 echo "a local service only." ;;
    "") echo "ERROR: OpenVAS Manager is NOT running!"
        echo "FIX: Start OpenVAS Manager (openvasmd)."
        OPENVASMD_PORT=-1 ;;
  esac
  case $OPENVASMD_PORT in
    -1) ;;
    9390) echo "OK: OpenVAS Manager is listening on port 9390, which is the default port." ;;
    *) echo "WARNING: OpenVAS Manager is listening on port $OPENVASMD_PORT, which is NOT the default port!"
       echo "SUGGEST: Ensure OpenVAS Manager is listening on port 9390." ;;
  esac
  
./openvasmd
#echo "13 - Open firewall port"
#SuSEfirewall2 open EXT TCP 9390
#SuSEfirewall2 stop
#SuSEfirewall2 start

echo "Script finished!"
