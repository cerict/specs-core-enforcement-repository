import sys
import os
import datetime
import json
import time
from threading import Thread
from utils import run_in_periods, send_post


class Monitoring:
    def __init__(self, extended_age_frequency, availability_frequency, scan_report_availability_frequency, ips, event_hub_url, django_url):
        self.reports = '/opt/'
        self.ips = ips
        self.extended_age_frequency = extended_age_frequency
        self.availability_frequency = availability_frequency
        self.scan_report_availability_frequency = scan_report_availability_frequency
        self.event_hub_url = event_hub_url
        self.django_url = django_url

    @run_in_periods
    def get_report_extended_age(self, report_extended_scan_frequency, delay=0, event_type='event'):
        if report_extended_scan_frequency > 0:
            return self._send_extended_age_event(event_type)

    @run_in_periods
    def pen_scanner_availability(self, frequency, delay=0, event_type='event'):
        return self._send_pen_scanner_availability(event_type)

    @run_in_periods
    def scan_report_availability(self, frequency, delay=0, event_type='event'):
        return self._scan_report_availability(event_type)

    def run(self):
        threads = [Thread(target=self.get_report_extended_age, args=(self.extended_age_frequency, )),
                   Thread(target=self.pen_scanner_availability, args=(self.availability_frequency, )),
                   Thread(target=self.scan_report_availability, args=(self.scan_report_availability_frequency, ))]

        for thread in threads:
            thread.daemon = True
            thread.start()

        while True:
            # We keep program running, so threads don't get killed, and wait for keyboard interrupt
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                sys.exit(0)

    def _scan_report_availability(self, event_type):
        for ip in self.ips:
            filename = '%s-scan.log' % ip
            file_available = os.path.isfile(os.path.join(self.reports, filename))
            self._send_event_report('sva_monitoring_adapter_???', 'sva_monitoring_adapter_???', [event_type], 'scan_report_availability', file_available, ip)

    def _send_pen_scanner_availability(self, event_type):
        libraries_exists = True
        if not os.path.exists(os.path.join(self.reports, 'openvas-libraries-cli')):
            libraries_exists = False
        if not os.path.exists(os.path.join(self.reports, 'openvas-libs')):
            libraries_exists = False
        if not os.path.exists(os.path.join(self.reports, 'openvas-xml-utilities')):
            libraries_exists = False
        self._send_event_report('sva_monitoring_adapter_???', 'sva_monitoring_adapter_???', [event_type], 'pen_scanner_availability', libraries_exists)

    def _send_extended_age_event(self, event_type):
        for ip in self.ips:
            filename = '%s-scan.log' % ip
            if os.path.isfile(os.path.join(self.reports, filename)):
                self._send_age_report('report_extended_age_msr3', os.path.getmtime(os.path.join(self.reports, filename)), event_type, ip)

    def _send_age_report(self, metric, age, event_type, ip):
        self._send_event_report('sva_monitoring_adapter_???', 'sva_monitoring_adapter_???', [event_type], metric, age, ip)

    def _send_event_report(self, component, obj, labels, type, value, ip=None):
        print "Component: %s, value: %s" % (component, value)
        timestamp = datetime.datetime.now()
        payload = {
            'component': component,
            'object': obj,
            'labels': labels,
            'type': type,
            'data': {
                'value': value,
                'ip': ip
            },
            'timestamp': time.mktime(timestamp.timetuple())
        }
        urls = [self.event_hub_url, self.django_url + 'sva_information/']
        json_payload = json.dumps(payload)
        for url in urls:
            send_post(url, data=json_payload)


if __name__ == '__main__':
    args = sys.argv
    age_frequency = int(args[2])
    scanner_availability = int(args[3])
    scan_report_availability = int(args[4])
    ips = eval(args[5])
    event_hub_url = args[6]
    django_url = args[7]
    monitoring = Monitoring(age_frequency, scanner_availability, scan_report_availability, ips, event_hub_url, django_url)
    if args[1] == 'monitor':
        monitoring.run()
    if args[1] == 'invoke_msr3':
        monitoring.get_report_extended_age(0, event_type='remediation_event')
    if args[1] == 'invoke_msr10':
        monitoring.pen_scanner_availability(0, event_type='remediation_event')
    if args[1] == 'invoke_msr8':
        monitoring.scan_report_availability(0, event_type='remediation_event')
