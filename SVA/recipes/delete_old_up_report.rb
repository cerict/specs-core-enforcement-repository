#
# Cookbook Name:: SVA
# Recipe id:: sva-r17
# Recipe:: Delete old up report
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

file '/tmp/openSUSE 13.2 Update/available_updates.json' do
	action :delete
end

file '/tmp/openSUSE 13.2 Update/available_updates.txt' do
	action :delete
end