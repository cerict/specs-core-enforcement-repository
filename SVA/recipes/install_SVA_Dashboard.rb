
# Cookbook Name:: SVA
# Recipe id:: sva-r6
# Recipe:: Install SVA Dashboard

# Copyright 2015, XLAB d.o.o

# All rights reserved - Do Not Redistribute
Chef::Config[:zypper_check_gpg] = false

if File.exist?('/tmp/dashboard') then
	puts "Dashboard recipe already ran"
	exit
end

package 'python-devel'
package 'rabbitmq-server' do
	version '3.1.5'
end
package 'nodejs'
package 'git'
package 'python-pip'
package 'python-virtualenv'
package 'nginx'

bash 'Install less' do
	code <<-EOH
		npm install -g less
		npm install -g yuglify
	EOH
	creates '/usr/bin/lessc'
end

include_recipe "postgresql::server"
include_recipe "database::postgresql"

projects_path = node['project_path']

service 'postgresql' do
	action :start
end

service 'rabbitmq-server' do
	action :start
	ignore_failure true
end

service 'rabbitmq-server' do	
	action :start
end

postgresql_connection_info = {
  :host     => '127.0.0.1',
  :port     => node['postgresql']['config']['port'],
  :username => 'postgres',
  :password => node['postgresql']['password']['postgres'],
}

postgresql_database 'dashboard' do
  connection postgresql_connection_info
  action     :create
end

postgresql_database_user 'dashboard' do
  connection postgresql_connection_info
  password 'dashboard'
  action :create
end

postgresql_database_user 'dashboard' do
  connection postgresql_connection_info
  database_name 'dashboard'
  privileges [:all]
  action :grant
end

ruby_block "Set python path variable" do
    block do
        Chef::Resource::RubyBlock.send(:include, Chef::Mixin::ShellOut)      
        command = 'which python'
        command_out = shell_out(command)
        node.set['python_path'] = command_out.stdout
    end
    action :create
end

user 'dashboard' do
	comment "User responsible for sva dashboard"
	system true
	home "#{projects_path}/specs-enforcement-sva-dashboard"
	shell "/bin/bash"
end

group 'dashboard' do
	members 'dashboard'
	append true
	system true
end

directory "#{projects_path}" do
	owner 'dashboard'
	group 'dashboard'
end

python_virtualenv "#{projects_path}/venv/dashboard" do
	group 'dashboard'
	user 'dashboard'
	python #{node['python_path']}
	not_if { ::File.directory?("#{projects_path}/venv/dashboard") }
end

git "#{projects_path}/specs-enforcement-sva-dashboard" do
	repository 'https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_dashboard.git'
	revision 'master'
	user 'dashboard'
	group 'dashboard'
	action :sync
end

directories = [
"#{projects_path}/specs-enforcement-sva-dashboard/logs", 
"#{projects_path}/specs-enforcement-sva-dashboard/bin", 
"#{projects_path}/specs-enforcement-sva-dashboard/run",
"/etc/nginx/sites-available",
"/etc/nginx/sites-enabled"
]

files = [
"#{projects_path}/specs-enforcement-sva-dashboard/logs/gunicorn_supervisor.log",
"#{projects_path}/specs-enforcement-sva-dashboard/logs/dashboard.log",
"#{projects_path}/specs-enforcement-sva-dashboard/logs/celery.log",
"#{projects_path}/specs-enforcement-sva-dashboard/logs/rabbitmq.log",
"/tmp/supervisor.log",

]

directories.each do |dir|
	directory dir do
		owner 'dashboard'
		group 'dashboard'
	end
end

files.each do |f|
	file f do
		owner 'dashboard'
		group 'dashboard'
	end
end

pip_requirements "#{projects_path}/specs-enforcement-sva-dashboard/dashboard/requirements.txt" do
	virtualenv "#{projects_path}/venv/dashboard"
end

python_package 'flower' do
	group 'dashboard'
	user 'dashboard'
	virtualenv "#{projects_path}/venv/dashboard"
end

python_execute 'Migrate' do
	command "#{projects_path}/specs-enforcement-sva-dashboard/dashboard/manage.py migrate"
	virtualenv "#{projects_path}/venv/dashboard"
	ignore_failure true
end

bash 'Install supervisor' do
	code <<-EOH
		pip install supervisor
	EOH
	creates '/usr/bin/supervisord'
end

bash 'Initial supervisor conf' do
	code <<-EOH
		echo_supervisord_conf > /etc/supervisord.conf
	EOH
end

template "#{projects_path}/specs-enforcement-sva-dashboard/bin/gunicorn_start.sh" do
  source 'gunicorn_start.erb'
  user 'dashboard'
  group 'dashboard'
  mode '0775'
  variables({
    :dashboard_dir => "#{projects_path}/specs-enforcement-sva-dashboard",
	:venv_dir => "#{projects_path}/venv/dashboard",
	:user => "dashboard",
	:group => "dashboard"
  })
end

template "/etc/dashboard.conf" do
  source 'supervisor.erb'
  user 'dashboard'
  group 'dashboard'
  variables({
    :dashboard_dir => "#{projects_path}/specs-enforcement-sva-dashboard",
	:user => "dashboard",
	:venv_dir => "#{projects_path}/venv/dashboard"
  })
end

ruby_block 'Configure supervisor conf file' do
	block do
		file = Chef::Util::FileEdit.new("/etc/supervisord.conf")
		file.insert_line_if_no_match("\A[include]", "[include]")
		file.insert_line_if_no_match("\Afiles = /etc/dashboard.conf", "files = /etc/dashboard.conf")
		file.write_file
	end
end

bash 'Add supervisor programs' do
	code <<-EOH
		supervisord
		supervisorctl reread
		supervisorctl update
		supervisorctl restart all
	EOH
end

template "/etc/nginx/nginx.conf" do
  source 'nginx.erb'
  user 'dashboard'
  group 'dashboard'
  variables({
    :dashboard_dir => "#{projects_path}/specs-enforcement-sva-dashboard",
	:user => "dashboard",
	:group => "dashboard"
  })
end

template "/etc/nginx/sites-available/dashboard" do
  source 'dashboard.erb'
  variables({
    :dashboard_dir => "#{projects_path}/specs-enforcement-sva-dashboard",
	:port => node['django']['port']
  })
end

template "/etc/init.d/supervisord" do
  source 'supervisord_init.erb'
  user 'root'
  group 'root'
  mode '0750'
  variables({
    :supervisord_path => "/usr/bin/supervisord"
  })
end

bash 'Make link between sites-available and sites-enabled' do
	code <<-EOH
		rm /etc/nginx/sites-enabled/dashboard
		ln -s /etc/nginx/sites-available/dashboard /etc/nginx/sites-enabled/dashboard
	EOH
end

bash 'Change nginx permissions' do
	code <<-EOH
		chown dashboard:dashboard -R /var/lib/nginx
	EOH
end

service "nginx" do
	action [ :enable, :start ]
end

python_execute 'Migrate' do
	command "#{projects_path}/specs-enforcement-sva-dashboard/dashboard/manage.py migrate"
	virtualenv "#{projects_path}/venv/dashboard"
	ignore_failure true
end

bash 'Add to chkconfig' do
	code <<-EOH
		chkconfig --add supervisord
		chkconfig --add nginx
	EOH
	ignore_failure true
end

file '/tmp/dashboard' do
	content 'recipe completed'
end





