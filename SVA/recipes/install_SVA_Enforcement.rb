#
# Cookbook Name:: SVA
# Recipe id:: sva-r4
# Recipe:: Install SVA Enforcement
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

## INSTALLING AND CONFIGURING POSTGRESQL

Chef::Config[:zypper_check_gpg] = false

if File.exist?('/tmp/enforcement.pid') then
	puts "Enforcement already running"
	exit
end

package 'postgresql-devel' do
	action :nothing
end.run_action(:install)

package 'screen'
package 'postgresql-devel'
package 'postgresql'
package 'postgresql-contrib'
package 'python-devel'
package 'python-pip'
package 'git'

## CONFIGURING POSTGRESQL

include_recipe "postgresql::server"
include_recipe "database::postgresql"

bash 'restart_postgresql' do
	cwd '/tmp'
	code <<-EOH
		if ! [ $(sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='sva'") == 1 ]; then
			service postgresql restart
		fi
	EOH
end

postgresql_connection_info = {
  :host     => '127.0.0.1',
  :port     => node['postgresql']['config']['port'],
  :username => 'postgres',
  :password => node['postgresql']['password']['postgres'],
}

postgresql_database 'sva' do
  connection postgresql_connection_info
  action     :create
end

postgresql_database_user 'sva' do
  connection postgresql_connection_info
  password 'sva'
  action :create
end

postgresql_database_user 'sva' do
  connection postgresql_connection_info
  database_name 'sva'
  privileges [:all]
  action :grant
end

## DONE INSTALLING AND CONFIGURING POSTGRESQL
projects_path = node['project_path']

include_recipe 'SVA::install_OpenSCAP'

## INSTALLING ENFORCEMENT COMPONENT

directory "#{projects_path}/specs_enforcement_sva" do
	recursive true
	action :delete
end

directory "#{projects_path}" do
	action :create
end

git "#{projects_path}/specs_enforcement_sva" do
	repository 'https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_vulnerability_manager'
	revision 'master'
	action :sync
end

git "#{projects_path}/specs_sva_core" do
	repository 'https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_core'
	revision 'master'
	action :sync
end

bash 'Install SVA Core' do
	cwd "#{projects_path}"
	code <<-EOH
		python -m pip install -e "specs_sva_core/"
	EOH
	ignore_failure true
end

bash 'Install Enforcement requirements' do
	cwd "#{projects_path}"
	code <<-EOH
		python -m pip install -r "specs_enforcement_sva/requirements.txt"
	EOH
end

## DONE INSTALLING ENFORCEMENT COMPONENT


## RUNNING ENFORCEMENT COMPONENT
require 'json'
require 'net/http'

def build_url(ip, port, additional='')
	return 'http://' + ip.to_s + ':' + port.to_s + '/' + additional.to_s
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first

if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end

slos = plan['slos']
sla_id = plan['sla_id']
component_id = ''
@plan_json = plan.to_json
File.open('/tmp/plan.json', 'w') { |file| file.write(@plan_json) }
scanning_frequency = 0
list_update_frequency = 0
up_report_frequency = 0
multiplier = 3600
vms = plan['pools'].first['vms']
django_ip = '0.0.0.0'
event_hub_ip = plan['monitoring_core_ip']
event_hub_port = plan['monitoring_core_port']

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['recipe'].include? 'install_SVA_Dashboard'
			django_ip = ip
		end
		if component['recipe'].include? 'install_SVA_Enforcement'
			component_id = component['component_id']
		end
	end
end

dashboard_url = build_url(django_ip, node['django']['port'])
event_hub_url = build_url(event_hub_ip, event_hub_port, 'events/specs')

@host = django_ip  # django
@port = node['django']['port']
@post_ws = "/plan/"
  

def post
	begin
		code = 404
		req = Net::HTTP::Post.new(@post_ws, initheader = {'Content-Type' =>'application/json'})
		req.body = @plan_json
		begin
			http = Net::HTTP.new(@host, @port)
			http.read_timeout = 10;
			response = http.request(req)
			code = response.code.to_i
		rescue Exception => e
			code = 404
			puts "Waiting for dashboard"
		end
		sleep(2)
	end until code == 200
end

post

slos.each do |slo|
	metric_id = slo['metric_id']
	value = slo['value'].to_i
	if metric_id == 'basic_scan_frequency_m13'
		scanning_frequency = value * multiplier
	end
	if metric_id == 'list_update_frequency_m14'
		list_update_frequency = value * multiplier
	end
	if metric_id == 'up_report_frequency_m23'
		up_report_frequency = value * multiplier
	end
end


bash 'configure_enforcement_database' do
	cwd "#{projects_path}"
	code <<-EOH
		python specs_enforcement_sva/src/enforcement.py set_dashboard_url #{dashboard_url}
	EOH
end


bash 'run_enforcement' do
	cwd "#{projects_path}"
	code <<-EOH
		screen -S enforcement -m -d python specs_enforcement_sva/src/enforcement.py run_enforcement #{scanning_frequency} #{list_update_frequency} #{up_report_frequency} 2>&1
	EOH
end

## DONE RUNNING ENFORCEMENT COMPONENT
