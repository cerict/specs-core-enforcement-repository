#
# Cookbook Name:: SVA
# Recipe id:: sva-r19
# Recipe:: Invoke SVA Monitoring to take measurement sva-mrs10 and label the event as remediation-event.
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

projects_path = node['project_path']

bash 'invoke_sva-msr10' do
	code <<-EOH
		cd #{projects_path}/specs_monitoring_sva/src
		python monitoring.py invoke_msr10
	EOH
end