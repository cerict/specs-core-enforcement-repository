#
# Cookbook Name:: SVA
# Recipe id:: sva-r8
# Recipe:: Reconfigure repository
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

projects_path = node['project_path']

bash 'reconfigure_repository' do
	code <<-EOH
		cd #{projects_path}/specs_enforcement_sva/src
		python enforcement.py reconfigure_repository
	EOH
end