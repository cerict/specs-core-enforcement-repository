#
# Cookbook Name:: SVA
# Recipe id:: sva-r16
# Recipe:: Reinstall scanners
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#


package 'openscap-utils' do
	action :remove
end

package 'openscap-utils'