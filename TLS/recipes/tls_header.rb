#
# Cookbook Name:: tls
# Recipe:: tls_header
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    TLS Header
##
##    It computes the common information from the implementation plan
######

require 'json'

#specs_plan_id = 'tls_tester'
specs_plan_id = node['implementation_plan_id']
specs_plan = search("implementation_plans","id:#{specs_plan_id}").first
if specs_plan == nil then
  puts "[ERROR] SPECS Implementation Plan with id: #{specs_plan_id} is missing from the Implementation databag."
  exit
end

impl_pool = specs_plan['pools'].first
impl_slos = specs_plan['slos']

puts specs_plan['slos']

tls_configurator_args = ""
tls_termintor_backend = ""

#### check if WebPool is among the recipes to be installed on this VM and set TLS Endpoint to localhost:1080 (WebPool http-in)
tls_endpoint = ""
impl_pool['vms'].each do |svm|
  svm['components'].each do |cmps|
    if cmps['component_id'] == 'wp_haproxy' then
      #if svm['public_ip'] = '' then
      #  puts "[ERROR] ['components'][0]['public_ip'] is empty. Please specify an IP address!"
      #  exit
      #end
      tls_endpoint = "127.0.0.1:1080"
    end
  end
  if tls_endpoint == "" then
    puts "[ERROR] TLS Endpoint was not found. Please define wp_haproxy component in your implementation plan"
    exit
  end
end
puts "[TLS Configurator] Endpoint IP: " << tls_endpoint
####

#### check enforceable metrics
impl_slos.each do |slo|
  if slo['metric_id'] == "tls_crypto_strenght_m3" then
    if ['geq', 'eq'].include? slo['operator'] then
      if slo['value'].to_i >= 7 then
        tls_configurator_args = tls_configurator_args + " --m3-plus"
      else
        tls_configurator_args = tls_configurator_args + " --m3"
      end
    elsif ['leq', 'lt'].include? slo['operator'] then
      if slo['value'].to_i <= 6 then
        tls_configurator_args = tls_configurator_args + " --m3"
      else
        tls_configurator_args = tls_configurator_args + " --m3-plus"
      end
    end
  elsif slo['metric_id'] == "forward_secrecy_m4" then
    if slo['value'] = "yes" then
      tls_configurator_args = tls_configurator_args + " --m4"
    end
  elsif slo['metric_id'] == "hsts_m5" then
    if slo['value'] = "yes" then
      tls_configurator_args = tls_configurator_args + " --m5"
    end
  elsif slo['metric_id'] == "https_redirect_m6" then
    if slo['value'] = "yes" then
      tls_configurator_args = tls_configurator_args + " --m6"
    end
  elsif slo['metric_id'] == "secure_cookies_m7" then
    if slo['value'] = "yes" then
      tls_configurator_args = tls_configurator_args + " --m7"
    end
  elsif slo['metric_id'] == "ceritificate_pinning_m10" then
    if slo['value'] = "yes" then
        tls_configurator_args = tls_configurator_args + " --m10"
    end
  end
end
####

#### add also TLS backend
tls_configurator_args += " --tls-backend " + tls_endpoint
tls_terminator_backend = " --tls-backend " + tls_endpoint
####

#### TLS Prober - component id
tls_component_id = ""
impl_pool = specs_plan['pools'].first
impl_pool['vms'].each do |svm|
  svm['components'].each do |cmps|
    if cmps['component_id'] == 'tls_prober' then
      tls_component_id = cmps['component_id']
      cmps['private_ips'].each do |prvip|
        tls_component_id += "-" + prvip
      end
    end
  end
  if tls_component_id == "tls_prober" then
    tls_component_id += "-" + svm['public_ip']
  end
end
####
puts "[TLS Header] Matched Prober ID: " << tls_component_id
puts "[TLS Header] Computed arguments: " << tls_configurator_args
puts "[TLS Header] Computed backend: " << tls_terminator_backend

node.default['tls_specs_plan'] = specs_plan
node.default['tls_configurator_args'] = tls_configurator_args
node.default['tls_terminator_backend'] = tls_terminator_backend
node.default['tls_component_id'] = tls_component_id
node.default['tls_sla_id'] = specs_plan['sla_id']
node.default['tls_slos'] = specs_plan['slos']
node.default['tls_measurements'] = specs_plan['measurements']
node.default['tls_monitoring_core_port'] = "#{specs_plan['monitoring_core_port']}"
node.default['tls_monitoring_core_ip'] = specs_plan['monitoring_core_ip']
#### TLS Terminator service
service "specs-mechanism-enforcement-tls-terminator" do
  provider Chef::Provider::Service::Systemd
  service_name "tls-terminator"
  action :nothing
  supports :restart => true, :start => true
end
####

#### TLS Prober service
service "specs-mechanism-enforcement-tls-prober" do
  provider Chef::Provider::Service::Systemd
  service_name "tls-adaptor"
  action :nothing
  supports :restart => true, :start => true
end
####
