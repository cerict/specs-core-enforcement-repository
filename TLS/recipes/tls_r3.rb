#
# Cookbook Name:: tls
# Recipe:: tls_r3
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    TLS Terminator Configurator -- Install
######

include_recipe "TLS::tls_r2"

specs_plan = node.default['tls_specs_plan']

impl_pool = specs_plan['pools'].first
impl_slos = specs_plan['slos']

tls_configurator_args = node.default['tls_configurator_args']
tls_termintor_backend = node.default['tls_termintor_backend']

bash "generate_tls_terminator_configuration_file" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    source /etc/profile
    /opt/specs-mechanism-enforcement-tls/bin/tls-configurator.sh #{tls_configurator_args}
    EOH
    notifies :restart, 'service[specs-mechanism-enforcement-tls-terminator]', :immediately
    returns [0, 1]
end

bash "update_tls_terminator_hostname_zone" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    source /etc/profile
    curl -s "http://solsys.ro/a/cloud-apps-ddns/?hostname=specs-tls&password=ritaledodedo"
    EOH
    returns [0, 1]
end
####
