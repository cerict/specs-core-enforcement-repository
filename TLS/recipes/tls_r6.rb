#
# Cookbook Name:: tls
# Recipe:: tls_r2
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    enforcement recipe -- Invoke TLS Prober to take measurement tls-msr1 and label the event as remediation-event.
######

include_recipe "TLS::tls_r2"

tls_r_prober_measurement = "tls_crypto_strength_level_msr1"
tls_r_prober_measurement_value = "7"
specs_slos = node.default['tls_slos']
specs_slos.each do |slo|
  if slo['metric_id'] == "tls_crypto_strenght_m3" then
    tls_r_prober_measurement_value = slo['value']
  end
end


#### DO NOT EDIT BELOW THIS LINE
# Usage: tls-adaptor.py cli [[object component data sla_id measurement monitoring_core_ip monitoring_core_port publish]]
tls_rf_prober_args = "cli '' " + node.default['tls_component_id'] + " " + tls_r_prober_measurement_value + " " + node.default['tls_sla_id']
tls_rf_prober_args += " " + tls_r_prober_measurement + " " + node.default['tls_monitoring_core_ip'] + " " + node.default['tls_monitoring_core_port'] + " 1"
puts "[tls_rX_prober] " + tls_rf_prober_args
bash "generate_tls_terminator_configuration_file" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    source /etc/profile
    /opt/specs-mechanism-enforcement-tls/sbin/tls-adaptor.py #{tls_rf_prober_args}
    EOH
    returns [0, 1]
end
puts "[TLS Enforcement] Running of tls_rX successful."
####
