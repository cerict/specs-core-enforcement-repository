WebPool Cookbook
====================
The mechanism, referred to as the Web Container Pool mechanism from now on, is based on the acquisition of a set of VMs (e.g. by using the Broker component described in the previous 
section), which are configured with different web containers instances in order to guarantee the level of diversity and the other security requirements negotiated with the End-user.
Together with the pool of web containers, an additional dedicated VM is deployed. It is configured to act as a balancer/proxy to properly forward the HTTP traffic towards the different 
web container instances, depending on traffic features and ongoing alerts/violation detections.
This cookbook contains three recipes: the apache one allows to install the apache2 Web Server, the nginx one allows to install the nginx Web Server,while the haproxy one allows to install the proxy.
Each one install memcached software to manage and keep the session alive.     

Requirements
------------

1) the target machines need the tcp port 80 and 11211
opened. On Amazon AMI: you need to:
- access to amazon console;
- add an inbound rule into the security group [Custom UDP rule].   
  
2) The target machine need the OpenSuse OS.

Attributes
----------
For each recipe,The attributes have to be passed a implementation plan id : 

{"implementation_plan_id":"ID_VALUE"}

Usage                              
-----------

#### create implementation plans data bag
if not exist, create a data bag "implementation_plans"  with follow command: knife data bag create data  

#### add implementation plan
add a item: knife data bag from file implementation_plan.json 

  example of  implementation_plan:

```
   {
  "id":"1154982",
  "context": {
    "slaID": "$SLA-ID",
    "planID": "$PLAN-ID",
    "annotations": [],
    "monitoring-core-ip": "192.168.1.150",
    "monitoring-core-port": "5000"
  },
  "IaaS": {
    "VMs": {
      "nodes-access-credentials-reference": "$$ filled by broker or $by plan",
      "nodes": [
        {
          "node-id": "ha-proxy-node",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": true,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "0.0.0.0/0",
                "source-nodes": [],
                "interface": "public",
                "proto": "TCP",
                "port-list": "22,80,443"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "0.0.0.0/0",
                "destination-nodes": [],
                "interface": "public,private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-1",
              "cookbook": "WebPool",
              "name": "haproxy",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.58.95"
          ],
          "public-ip": "$$ filled by broker"
        },
        {
          "node-id": "apache-node1",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22,80,443"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-3",
              "cookbook": "WebPool",
              "name": "apache",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.60.225"
          ],
          "public-ip": "$$ filled by broker"
        },
        {
          "node-id": "apache-node2",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22,80,443"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-5",
              "cookbook": "WebPool",
              "name": "apache",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.51.223"
          ],
          "public-ip": "$$ filled by broker"
        },
        {
          "node-id": "nginx-node",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22,80,443"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-7",
              "cookbook": "WebPool",
              "name": "nginx",
              "implementation-phase": 1
            },
          ],
          "private-ips": [
            "172.31.48.150"
          ],
          "public-ip": "$$ filled by broker"
        }
      ]
    },
    "STORAGEs": []
  },
  "providers": [
    {
      "id": "aws-east",
      "name": "aws-ec2",
      "zone": "us-east-1"
    }
  ],
}
```
#### WebPool::apache

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['WebPool:apache']' -j '{ "implementation_plan_id":"<id_value>"}' 

example : knife bootstrap 192.168.1.101 -x root -P specs --node-name apachenode1 --run-list 'recipe['WebPool:apache']' -j '{ "implementation_plan_id":"1154982"}'

#### WebPool::nginx

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['WebPool:nginx']' -j '{ '{ "implementation_plan_id":"<id_value>"}'}' 

example : knife bootstrap 192.168.1.103 -x root -P specs --node-name nginxnode --run-list 'recipe['WebPool:apache']' -j '{ "implementation_plan_id":"1154982"}'

#### WebPool::haproxy

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['WebPool:haproxy']' -j '{ '{ "implementation_plan_id":"<id_value>"}'}'

example : knife bootstrap 192.168.1.103 -x root -P specs --node-name nginxnode --run-list 'recipe['WebPool:haproxy']' -j '{ "implementation_plan_id":"1154982"}'


Contributors
------------------
Vincenzo Cinque 


License and Authors
-------------------
Authors: Cinque Vincenzo 
License :
//# Copyright (C) 2014 - 2016 CeRICT
//# Developed for the SPECS Project FP7 - GA 610795 - 
//# Authors: 
//# %%
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#      http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
