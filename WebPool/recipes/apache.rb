#
# Cookbook Name:: WebPool
# Recipe:: apache
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe"mysql::default"
# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
# plan = search("implementation_plans","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then 
puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
exit
end

# install apache2

package 'apache2' do
action :install
end

# install php5

package 'apache2-mod_php5' do
action :install
end

package 'php5-mysql' do
action :install
end

package 'php5-zip' do
action :install
end

package 'php5-curl' do
action :install
end

# Edit apache2 configuration file

cookbook_file  "apache2" do
   path "/etc/sysconfig/apache2"
   action :create
end

cookbook_file  "httpd.conf" do
   path "/etc/apache2/httpd.conf"
   action :create
end

cookbook_file  "default-server.conf" do
   path "/etc/apache2/default-server.conf"
   action :create
end

cookbook_file  "mod_log_config.conf" do
   path "/etc/apache2/mod_log_config.conf"
   action :create
end

service 'apache2.service' do
action [:enable,:start]
end

# install memcached

package 'memcached' do
action :install
end

# install dependence php-memcached

package 'gcc' do
action :install
end

package 'make' do
action :install
end

package 'php5-devel' do
action :install
end

# Download  php-memcached package
 
cookbook_file  "memcache.tgz" do
   path "/tmp/memcache.tgz"
   action :create
end

# unzip tar file

bash "install_memcache" do
        user "root"
        cwd  "/etc"
        code <<-EOH
        tar -xvzf /tmp/memcache.tgz
        rm /tmp/memcache.tgz
 	EOH
        not_if { ::File.exists?("/etc/package.xml") }
end

# install php-memcached

bash "install_memcache" do
        user "root"
        cwd "/etc/memcache-2.2.7"
        code <<-EOH
        	phpize
		./configure
		make install
	EOH
	end
 
# Edit php5-memcache configuration file
 
template "/etc/php5/apache2/php.ini" do
        action :create
        source "php.ini.erb"
	variables(
	:plan => plan
	)
end

template "/etc/hosts" do
        action :create
        source "hosts.erb"
	variables(
	:plan => plan
	)
end

# Install Web Application

cookbook_file  "index.php.apache" do
   path "/srv/www/htdocs/index.php"
   action :create
end

# install app for manage web app deployed

directory '/srv/www/htdocs/manage_app' do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

cookbook_file "index.php.webapp" do
   path "/srv/www/htdocs/manage_app/index.php"
   owner 'root'
   group 'root'
   mode '0777'
   action :create
end

directory '/srv/www/htdocs/manage_app/uploads' do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

cookbook_file "unzip.php" do
   path "/srv/www/htdocs/manage_app/uploads/unzip.php"
   owner 'root'
   group 'root'
   mode '0777'
   action :create
end
 

# Run Apache2-php5-memcached service

bash "execute_programm" do
        user "root"
        code <<-EOH
       
        # enable memcached
	/usr/sbin/memcached -u root -l 0.0.0.0 -p 11211 -M -m 64 -d	

	#path to pgrep command
	PGREP="/usr/bin/pgrep"
	HTTPD="httpd"
	# find httpd pid
	$PGREP ${HTTPD}
	if [ $? -ne 0 ] # if apache not running
	then
   		systemctl start apache2.service
	else
		systemctl restart apache2.service
	fi
        EOH
	end



