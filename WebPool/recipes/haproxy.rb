#
# Cookbook Name:: WebPool
# Recipe:: haproxy
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe "enabling-platform::apache-tomcat-v7"

# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
# plan = search("implementation_plans","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then
    puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
    exit
end

# Install netcat

package 'netcat-openbsd' do
    action :install
end

# Download package HAProxy

cookbook_file  "haproxy-hatop.tar.gz" do
    path "/tmp/haproxy-hatop.tar.gz"
    action :create
end

# unzip tar file

bash "install_program" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    tar -zxvf /tmp/haproxy-hatop.tar.gz
    rm /tmp/haproxy-hatop.tar.gz
    EOH
    not_if { ::File.exists?("/opt/haproxy") }
end

# Edit haproxy configuration file

template "/opt/haproxy/configs/haproxy.cfg" do
    action :create
    source "haproxy_config.erb"
    variables(
              :plan => plan
              )
end

# Map Hostname - IP address

template "/etc/hosts" do
    action :create
    source "hosts.erb"
    variables(
              :plan => plan
              )
end

cookbook_file  "haproxy.conf" do
   path "/etc/rsyslog.d/haproxy.conf"
   action :create
end

bash "remove_adapter_log_file" do
    user "root"
    code <<-EOH
    rm /opt/webpool-adapter.log
    EOH
    only_if { ::File.exists?("/opt/webpool-adapter.log") }
end



bash "kill_process_adapter" do
    user "root"
    code <<-EOH
    ps axf | grep webpool-adapter | grep -v grep | awk '{print "kill -9 " $1}' | sh
    EOH
end


bash "kill_process_haproxy" do
    user "root"
    code <<-EOH
    ps axf | grep /opt/haproxy-1.5.9/haproxy | grep -v grep | awk '{print "kill -9 " $1}' | sh
    EOH
end

# Run HAProxy service

bash "execute_programm" do
    user "root"
    code <<-EOH
    rm -f /opt/haproxy/haproxysock
    /opt/haproxy-1.5.9/haproxy -f /opt/haproxy/configs/haproxy.cfg
    EOH
end

bash "restart_rsyslog" do
    user "root"
    code <<-EOH
    service rsyslog restart
    EOH
end

# load war for DeployApp on tomcat

cookbook_file 'DeployApp.war' do
    path "/opt/mos-apache-tomcat-v7/webapps/DeployApp.war"
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    only_if {! ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/DeployApp") }
end


# Download java jdk 7

remote_file  "/tmp/java.tar.gz" do
    source "http://www.java.net/download/jdk7u80/archive/b05/binaries/jdk-7u80-ea-bin-b05-linux-x64-20_jan_2015.tar.gz"
    not_if { ::File.exists?("/opt/jdk1.7.0_80/") }
end

# install jdk

bash "install_jdk" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    
    tar xvzf /tmp/java.tar.gz
    rm /tmp/java.tar.gz
    echo 'export JAVA_HOME=/opt/jdk1.7.0_80/' >> /root/.bashrc
    #set PATH
    echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /root/.bashrc
    
    EOH
    not_if { ::File.exists?("/opt/jdk1.7.0_80/") }
end

# get EventHub information
EventHubIP=plan['monitoring_core_ip']
EventHubPort=plan['monitoring_core_port']
RefreshPeriod="60000"

PlanID=plan['plan_id']
puts "id plan:"
puts "#{PlanID}"

SlaID=plan['sla_id']
puts "id sla:"
puts "#{SlaID}"

PoolsString= plan['pools'].first.to_json.to_s
PoolsStringEscape= PoolsString.gsub("\"", "\\\"")

SlosString=plan['slos'].to_json
SlosStringEscape= SlosString.gsub("\"", "\\\"")

MeasurementsString=plan['measurements'].to_json
MeasurementsStringEscape= MeasurementsString.gsub("\"", "\\\"")

puts "pools object plan:"
puts "#{PoolsString}"
puts "pools object escape plan:"
puts "#{PoolsStringEscape}"

puts "slos object plan:"
puts "#{SlosString}"
puts "slos object escape plan:"
puts "#{SlosStringEscape}"

puts "measurements object plan:"
puts "#{MeasurementsString}"
puts "measurements object escape plan:"
puts "#{MeasurementsStringEscape}"

PlanConcat = "\"#{PlanID}"+"split_string"+"#{SlaID}"+"split_string"+"#{PoolsStringEscape.to_s}"+"split_string"+"#{SlosStringEscape.to_s}"+"split_string"+"#{MeasurementsStringEscape.to_s}\""
puts "planConcat object plan:"
puts "#{PlanConcat}"


if EventHubIP == nil or EventHubPort == nil
    puts "Insert monitoring-core-ip and monitoring-core-port in context tag of implementation plan"
    exit
end

cookbook_file "webpool-adapter.jar" do
    path "/opt/webpool-adapter.jar"
    action :create
end

bash "execute_webpool_adapter" do
    
    user "root"
    cwd  "/opt"
    environment 'PATH' => "#{ENV['PATH']}:/opt/jdk1.7.0_80/bin"
    code <<-EOH
    
    java -jar webpool-adapter.jar #{PlanConcat} #{EventHubIP} #{EventHubPort} #{RefreshPeriod} > webpool-adapter.log &
    
    
    
    EOH
    not_if { ::File.exists?("/opt/webpool-adapter.log") }
end